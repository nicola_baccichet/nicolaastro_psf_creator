function [PSF]=calculate_PSF(spr_path,spr_name)
% PSF calculator script
% author: Nicola Baccichet, nicola.baccichet.12@ucl.ac.uk
% version: 2.2
% last modified: 01/06/2015
% clear all; close all;clc;
% path with functions used
addpath golay_func/
warning('off','images:initSize:adjustingMag')   % clear from warning messages
%% sources definition
% load variables
spreadsheet_variables = load_spreadsheet_params(spr_path,spr_name,'PSF_create')
% set variables
lambda = spreadsheet_variables.lambda;    % [um]
f = spreadsheet_variables.f;              % [um] focal lenght
zxp = spreadsheet_variables.zxp;          % [um] propagrion distance 
w = spreadsheet_variables.w;              % [um] single aperture diameter
ds = spreadsheet_variables.ds;            % [um] distance between aperture pairs (g3/6) - scale of the coordinates (for non symmetrical arrays)
G = spreadsheet_variables.G;

lz = lambda*f;
%% get apertures coordinates and define number of points for calculation
[coordinates,centroid_pos] = golay_point_coordinates(G,'centroid');

% set encircling radius
if strcmp(G,'g3')==1 || strcmp(G,'g6')==1
    D = sqrt(3)/2*ds+w;     % [um]
else
    all_dist = pdist([centroid_pos;coordinates]);
    D = max(all_dist(1:size(coordinates,1)))*ds+w/2;
end

scale = 50;
L = scale*D;
M = fix(L^2/lz);        % critical sampling condition
% check if number is even
M = makeEven(M);
% M = 2^10;             % fixed sampling - uncomment if needed
totalRAM = 10*(8*M^2/1e9) + 3*(8*M/1e9); %in GB - considered three 2D matrix more to account also plots
%% control on memory used
control = input(['total RAM needed ' num2str(totalRAM) ' GB, type (yes) to allow execution: '],'s');
while strcmp(control,'yes')==0
%     disp('execution stopped')
%     break
%     return
    newscale = input(['current zero-pad factor ' num2str(scale) '. Enter new value: ']);
    L = newscale*D;
    M = fix(L^2/lz);        % critical sampling condition
    % check if number is even
    M = makeEven(M);
    totalRAM = 10*(8*M^2/1e9) + 3*(8*M/1e9); %in GB - considered three 2D matrix more to account also plots
    control = input(['total RAM needed ' num2str(totalRAM) ' GB, type (yes) to allow execution: '],'s');
    scale = newscale;
end
%% inizialize mesh
[dx,x,X,Y,fx]=create_mesh(M,L);  % [um]
[dx,lz/L]   % check on fresnel number
%% initialize pupils
% rotate and flip pupil if needed
rot_control = input('enter (y) if the pupil needs to be rotated: ','s');
if strcmp(rot_control,'y')==1
    rtang = input('enter the rotation angle in degrees: ','s');
    rtang = str2double(rtang);

    t = degtorad(rtang);
    R = [
            cos(t)      -sin(t);
            sin(t)      cos(t)
        ];

    coordinates = coordinates*R;
end
% calculate pupil
disp('initialising pupil...')
golay_array = pupil_function(X,Y,w,ds,coordinates); clear X Y

% display pupil if desired
plot_control = input('enter (y) to show entrance pupil - with encircling radius highlighted in red: ','s');
if strcmp(plot_control,'y')==1
    figure,imagesc(x,x,golay_array)
    truesize(gcf)
    colormap gray
    lim = 500; xlim([-lim lim]),ylim([-lim lim])
    xlabel('\mum'),ylabel('\mum'),
    title([G ' Entrance pupil'])
    hold on
    viscircles(centroid_pos,D);
end
clear plot_control
%% propagation
disp('calculating PSF...')
[PSF,L1,x1,dx1]=lens_propagation_v3(golay_array,w,L,lambda,zxp,f);
% show PSF if desired
plot_control = input('enter (y) to show the PSF: ','s');
if strcmp(plot_control,'y')==1
    I = abs(PSF).^2;    I = I/max(max(I));	%clear PSF;
    edges=4000*1e-3;
    showimage(x1*1e-3,x1*1e-3,I,1); xlim([-edges edges]),ylim([-edges edges]), xlabel('[mm]')
    truesize(gcf)
    title([G ' 2D PSF at ' num2str(zxp*1e-3) ' mm'])
end
clear plot_control
% show 1D plot if desired
plot_control = input('enter (y) to plot PSF along the center: ','s');
if strcmp(plot_control,'y')==1
    figure,plot(x1*1e-3,I(fix(M/2),:)); xlim([-edges edges]), xlabel('[mm]')
    title(['PSF profile along the centre at ' num2str(zxp*1e-3) ' mm'])    
end
clear plot_control
%% create MTF
mtf_control = input('enter (y) calculate and display MTF (needed only for display purposes): ','s');
if strcmp(mtf_control,'y')==1
    I = abs(PSF).^2;    I = I/max(max(I));	%clear PSF;
    MTF = ifftshift(fft2(fftshift(I)));
    MTF = real(MTF);
    showimage(fx,fx,MTF.^2,4);
    truesize(gcf)
    mtf_lim = 0.03; xlim([-mtf_lim mtf_lim]); ylim([-mtf_lim mtf_lim]);
    title([G ' MTF']),xlabel('f_x (cyc/um)'),ylabel('f_y (cyc/um)')
    clear mtf_control
    % 3D MTF plot
    mtf_control = input('enter (y) to plot MTF in 3D - NOT RECOMMENDED, MIGHT CRASH MATLAB: ','s');
    if strcmp(mtf_control,'y')==1
    figure
    surf(fx,fx,MTF,'FaceColor','texturemap','FaceLighting','gouraud')
    xlim([-mtf_lim mtf_lim]); ylim([-mtf_lim mtf_lim]);
    camlight left; lighting phong; shading interp
    end
end
%% save selected variables
control_on_data_save = input('enter (y) to save the data needed to perform image reconstruction: ','s');
if strcmp(control_on_data_save,'y')==1
    disp('saving variables...')
    save([G '_grids.mat'],'x','x1','dx','dx1','L','L1','f','lambda','w','ds','G','M','-v7.3');
    save([G '_PSF.mat'],'PSF','-v7.3')
    disp('Done')
end
rmpath golay_func/