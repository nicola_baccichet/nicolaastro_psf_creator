function [smallPSF]=visualize_and_crop(spr_path,spr_name,PSF)
% image cropping script
% author: Nicola Baccichet, nicola.baccichet.12@ucl.ac.uk
% version: 2.1
% last modified: 02/06/2015
% clear all; close all; clc;
%% loads data - for standalone script - uncomment if needed
% disp('loading previously calculated PSF')
% D = '7ap_lam';
% load([D '_grids.mat']); % load paramteres needed for cropping
% name=[D '_PSF'];        % load calculated PSF
% load([name '.mat']);    % load selected file
% width = 1024;
% height = 1024;
% px = 13;   % um - pixel size
%% loads data - with full script
spreadsheet_variables = load_spreadsheet_params(spr_path,spr_name,'PSF_crop')
D = spreadsheet_variables.D;
load([D '_grids.mat']); % load paramteres needed for cropping
width = spreadsheet_variables.width;
height = spreadsheet_variables.height;
px = spreadsheet_variables.px;   % um - pixel size
%% define cut area

center = fix(M/2);

Ln = width*px;      % side length
Mn = round(Ln/dx);    % number of points

% set limits for rows/columns cropping
x_min = center-round(Mn/2);
x_max = center+round(Mn/2);
y_min = x_min;
% resize coordinates
xn = x(center-round(Mn/2):center+fix(Mn/2));
%% crop the image
disp('cropping...')
PSF = PSF(x_min:x_max,x_min:x_max);
%% resize the image to same size of data images
disp('resizing...')
% OLD CROPPING METHOD
% smallPSF = imresize(PSF,[width height],'nearest');
% NEW with cropping in fourier space
psf = abs(PSF).^2;
otf = ifftshift(fft2(fftshift(psf)));            clear psf
% get new center coordinates
newcenter = fix(size(otf,1)/2);
% set new limits for rows/columns cropping
fx_min = newcenter-width/2-1;
fx_max = newcenter+width/2-2;
% crop otf
otf = otf(fx_min:fx_max,fx_min:fx_max);
% go back to psf
smallPSF = ifftshift(ifft2(fftshift(otf))); clear otf
smallPSF = abs(smallPSF);
%% plot images if desired
plot_control = input('Enter (y) to show initial and final PSFs: ','s');
if strcmp(plot_control,'y')==1
    % show new image (cropped, unscaled)
    figure
    imagesc(xn*1e-3,xn*1e-3,abs(PSF).^2)
    xlabel('x [mm]'),ylabel('y [mm]')
    title('cropped, unscaled PSF')
    % show new image (cropped, scaled)
    figure
    imagesc(xn*1e-3,xn*1e-3,abs(smallPSF).^2)
    xlabel('x [mm]'),ylabel('y [mm]')
    title('cropped, scaled PSF')
end
%% save cropped image with variables
disp('saving...')
save(['cropped_' G '_PSF.mat'],'PSF','dx','Ln','Mn','lambda','G','ds','f','w','smallPSF')
disp('cropped data saved')