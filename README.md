# README #

DISCLAIMER: this is a very preliminary version. A number of settings need to be changed accordingly to make it work properly.
Please contact the author for a full explanation on how to use this and what to expect.

Author: Nicola Baccichet

Date: 14 December 2015

Email: nicola.baccichet.12@ucl.ac.uk

This is a very alpha version of an image reconstruction algorithm made in MATLAB.
You will need the following toolboxes to run it properly:
image_toolbox
matlab
phased_array_system_toolbox
statistics_toolbox

### What is this repository for? ###

* Quick summary

This program allows to calculate the PSF of a focal-plane interferometric system with n-apertures, at a specified wavelength.
It is assumed that the detector is an array with a certain number of pixels.
Given the current architecture, the software is extremely limited in memory and allows to simulate only small systems.
It requires inputs on parameters such as operating wavelength, aperture geometry, detector feature.

* Version: alpha.1

### How do I get set up? ###

* Summary of set up

1. save all files in a folder of preference

2. make sure all the files contained also in the subfolder 'golay_func' are copied in the proper location

3. enter manually the apertures coordinate system in the file located inside golay_func/ called 'golay_point_coordinates.m', the format needs to be the same as the other coordinate sets already present

4. enter the system parameter in the excel spreadsheet contained in the main folder

5. run calculate_PSF. with inputs path and filename of the .xlsx file

6. run visualize_and_crop.m to adapt the PSF to the detector size and number of pixels

### Contribution guidelines ###

* Writing tests: Feel free to contact the author for a full explanation on how to setup and run the script.
* Code review: Any suggestions welcome
* Other guidelines: A demonstration on what this program produces can be found here: https://www.ucl.ac.uk/fisica-london-workshop/pdfs/abstract-6-extended

### Who do I talk to? ###

* Repo owner or admin: 

Nicola Baccichet, nicola.baccichet.12@ucl.ac.uk

* Other community or team contact