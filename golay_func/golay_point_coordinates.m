function [coordinates,centroid_pos,molt_dist] = golay_point_coordinates(n,reference)
% provides the coordinates of aperture centers of golay configurations 3, 6
% and linear array (5 apertures) and mask designs for LAM nanosatellite
% reference specifies the reference system (center of axis or centroid)
% author: Nicola Baccichet, nicola.baccichet.12@ucl.ac.uk
% version: 2.1
% last modified: 21/04/2015
%%
switch n
    case 'g6'
% revised from miller2007
         c_00 = [
             1/2    0;
             3/4    0;
             1/8    sqrt(3)/8;
             1/4    sqrt(3)/4;
             5/8    3*sqrt(3)/8;
             3/4    sqrt(3)/4
             ];
% need to shift respect to triangle center of gravity
        centroid_pos = [sum(c_00(:,1))/6 ,   sum(c_00(:,2))/6]; 
% centered on position (0,0)
        c = [
            0       -1/(2*sqrt(3));
            1/4     -1/(2*sqrt(3));
            1/4     sqrt(3)/12;
            1/8     5*sqrt(3)/24;
            -1/4    sqrt(3)/12;
            -3/8    -sqrt(3)/24;
         ];
     molt_dist = 2*cos(pi/6);   % needed for pupil mapping script
    case 'g3'
         c_00 = [
            -0.5    -sqrt(3)/6
            0.5     -sqrt(3)/6
            0       sqrt(3)/3
            ];
% need to shift respect to triangle center of gravity
        centroid_pos = [sum(c_00(:,1))/3 ,   sum(c_00(:,2))/3]; 
% centered on position (0,0)
        c = c_00;
      molt_dist = 2*cos(pi/6);   % needed for pupil mapping script
    case 'kjetil_old'
        c_00 = [
            -0.311	-0.398;
            -0.449	-0.077;
            -0.270	-0.138;
            0.023	-0.148;
            0.366	-0.189;
            0.222	0.453;
            0.401	0.308;
                ];
        centroid_pos = [sum(c_00(:,1))/7 ,   sum(c_00(:,2))/7];
        % centered on position (0,0)
        c = c_00;
      molt_dist = 2*cos(pi/6);   % needed for pupil mapping script
% case LAM 
    case 'triplet_assym'
        c_00 = [
            -0.025	-0.05;
            0.025	0.000;
            0.000	0.05;
                ];
        centroid_pos = [sum(c_00(:,1))/3 ,   sum(c_00(:,2))/3];
        % centered on position (0,0)
        c = c_00;
      molt_dist = 2*cos(pi/6);   % needed for pupil mapping script
    case 'linear'
        c =  [  
        0 0;
        4 0;
        1 0;
        2 0;
        6 0;
        ];

            coordinates = zeros(size(c));
            coordinates(1,1)=c(1,1);
            for i = 2:length(c)
                coordinates(i,1) = coordinates(i-1,1)+c(i,1);
            end
            coordinates(:,1) = coordinates(:,1)./max(coordinates(:,1));
            centroid_pos = sum(coordinates(:,1))/length(coordinates(:,1));
            coordinates(:,1) = coordinates(:,1)-ones(length(coordinates),1)*centroid_pos; 
            c = coordinates;
    case '7ap_lam'
        % original coordinates in um
            c_00 = [
0.221	-0.079;
0.36	-0.152;
0.385	-0.247;
0.334	-0.344;
0.169	-0.398;
0.087	-0.401;
0	0
];
        centroid_pos = [sum(c_00(:,1))/7 ,   sum(c_00(:,2))/7];
        % re centering on (0,0)
        c = c_00 - repmat(centroid_pos,7,1);
        centroid_pos = [sum(c(:,1))/7 ,   sum(c(:,2))/7];
end
% valid only for Golay-6
if strcmp(reference,'centroid')==1
    coordinates = c;
elseif strcmp(reference,'axis_center')==1
    coordinates = c_00;
else
    disp('invalid reference system -- retype')
    return
end
return