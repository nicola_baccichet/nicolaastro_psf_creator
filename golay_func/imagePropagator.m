function uout=imagePropagator(uin,wxp,zxp,lambda,L,display)
% nyquist frequency needs to be ok --> du <= lambda*(f/#)/2
% ----------------------------------------------------------
%% define grid
M = size(uin);
du = L/M;
f0 = wxp/(lambda*zxp);
fN = 1/(2*du);
if 2*f0 > fN
    fprintf('\n warning, sampling criteria is not satisfied \n \n')
    f0
    return
end
fu  =-1/(2*du):1/L:1/(2*du)-(1/L);
[Fu,Fv] = meshgrid(fu,fu); clear fu
H = circ(radius(Fu,Fv)/f0);
if strcmp(display,'on')==1
    figure
    surf(fu,fu,H.*.99)
    camlight left; lighting phong
    colormap('gray')
    shading interp
    ylabel('fu (cyc/m)'); xlabel('fv (cyc/m)');
end
%% propagate
H=fftshift(H);
Gg=fft2(fftshift(uin)); clear uin
Gi=Gg.*H;   clear Gg H
uout=ifftshift(ifft2(Gi)); clear Gi
return