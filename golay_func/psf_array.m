function psf = psf_array(u,v,N,lambda,del_kx,del_ky,f,D)
% analytical expression for the psf array (to check this)
% from FIETE2002
subsum = 0;
for k = 1:length(del_kx)
    subsum = subsum + cos(2*pi/(lambda*f).*(del_kx(k).*u+del_ky(k).*v));
end
% size(subsum)
% size(psf_sub(u,v,D,lambda,f))
% find(psf_sub(u,v,D,lambda,f)==NaN)
psf = psf_sub(u,v,D,lambda,f).*(N + 2*subsum);
% find(psf==NaN)
return