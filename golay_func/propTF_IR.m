function [u2,x] = propTF_IR(u1,L,lambda,z)
% propagation - impulse response approach - Fresnel sampling
% assumes same x and y side lengths and
% uniform sampling
% u1 - source plane field
% L - source and observation plane side length
% lambda - wavelength
% z - propagation distance
% u2 - observation plane field
[M,~]=size(u1);             %get input field array size
dx=L/M;                     %sample interval
k=2*pi/lambda;              %wavenumber
x=-L/2:dx:L/2-dx;           %spatial coords
[X,Y]=meshgrid(x,x);

h=1/(1i*lambda*z)*exp(1i*k/(2*z)*(X.^2+Y.^2)); clear X Y  %impulse
H=fft2(fftshift(h))*dx^2;  clear h              %create trans func

U1=fft2(fftshift(u1));
U2=H.*U1;       clear H U1
u2=ifftshift(ifft2(U2));
return