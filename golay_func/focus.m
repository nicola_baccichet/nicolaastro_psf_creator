function[uout]=focus(uin,L,lambda,zf)
% converging or diverging phase-front
% uniform sampling assumed
% uin - input field
% L - side length
% lambda - wavelength
% zf - focal distance (+ converge, - diverge)
% uout - output field
M=size(uin,1);
dx=L/M;
k=2*pi/lambda;
x=-L/2:dx:L/2-dx;
[X,Y]=meshgrid(x,x); clear x
uout=uin.*exp(-1i*k/(2*zf)*(X.^2+Y.^2)); %apply focus
return