function [dx,x,X,Y,fx,FX,FY]=create_mesh(M,L)
% returns the meshes for spatial coordiantes and spatial frequencies
%% spatial coordinates
% assumed x=y
dx = L/M;
x = -L/2:dx:L/2-dx;
[X,Y]=meshgrid(x,x);
%% spatial frequencies
fx=-1/(2*dx):1/L:1/(2*dx)-(1/L);
% [FX,FY] = meshgrid(fx,fx);
return