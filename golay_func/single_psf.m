function psf=single_psf(u,U,aperture_diameter,lambda,focal_plane_distance,...
    contrast)
% subinterface that gives the plot and psf of a defined singular aperture
M = size(U);
psf = psf_sub(U,U,aperture_diameter,lambda,focal_plane_distance);
psf = psf/max(max(psf));    % normalize psf
psf = nthroot(psf,contrast);       % enhance psf contrast
figure
imagesc(u,u,psf)
colormap gray
figure
plot(u,psf(M/2,:))
return