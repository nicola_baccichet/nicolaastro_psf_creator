function [gridval,L_new,dx_new] = createGrid(text,sizeOfField,Lin,lambda,z)
% THE MESH IS NOT CREATED IN THIS SCRIPT
% calculate the size of new field after fresnel propagation
sizeOfField = sizeOfField(1);
dxin = Lin/sizeOfField;

switch text
    case 'Fraunhofer'
        lz = lambda*z;
        L_new = lz/dxin;
        dx_new = lz/Lin;
        gridval = -L_new/2:dx_new:L_new/2-dx_new;
    case 'Fresnel'
        L_new=Lin;
        gridval = -L_new/2:dxin:L_new/2-dxin;
        dx_new = dxin;
end
% THE MESH IS NOT CREATED IN THIS SCRIPT
return