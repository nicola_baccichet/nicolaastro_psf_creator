function [psf]=psf_sub(x,y,D,lambda,f)
% evaluates the psf of a circular aperture of
% D = aperture diameter
% f = distance between pupil and focal plane
% lambda = lambda
% psf equation from FIETE2002
% ---------------------------------
r = sqrt(x.^2+y.^2);
cost = ( pi.*D.^2./(4.*lambda.*f) ).^2;
arg = pi.*D.*r./(lambda.*f);
% computes for arg=0
mask=(arg~=0);
psf = cost .* ( pi*ones(size(arg))/3 ).^2;
% computes for the rest
J1(mask) = besselj(1,arg(mask));
psf(mask) = cost .* ( 2.*J1(mask)./arg(mask)' ).^2;
return