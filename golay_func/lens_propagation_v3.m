function[uout,Lnew,xout,dxnew]=lens_propagation_v3(uin,w,L,lambda,d,f)
% wave propagation through lens @ its focal plane f
% assumes uniform sampling
% L - source plane side length
% lambda - wavelength
% d - propagation distance
% f - focal length
% Lnew - observation plane side length
% xout - observation plane field
% w - radius of the propagated beam (aperture radius)
% ---------------------------------------------------
%% do the focalization (multiplies the corresponding exponential term to field)
ufoc = focus(uin,L,lambda,f); clear uin
% showimage(x,x,abs(ufoc).^2,1);
%% propagate wavefront to focus

[uout,Lnew,xout,~,~] = propagate(ufoc,L,lambda,d,w,0.01); clear ufoc

dxnew = abs(xout(2)-xout(3));
return