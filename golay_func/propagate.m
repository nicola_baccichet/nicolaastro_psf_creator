function [uout,Lout,xout,Nf,typeOfpropagation]=propagate(uin,Lin,lambda,z,w,treshold)
% evaluate the type of propagation to perform basing on the Fresnel number
% value.
% Nf is compared with a treshold value, defined by the user
M = size(uin,1); dx = Lin/M; lz = lambda*z;
Nf = w^2 / (lambda*z);      %fresnel number

[dx,lz/Lin,Nf]

if Nf >= treshold
    if dx >= lz/Lin
        [uout,xout,Lout]=propTF(uin,Lin,lambda,z); clear uin
        disp 'Fresnel propagation was used'
    else
        [uout,xout]=propTF_IR(uin,Lin,lambda,z); clear uin
        disp 'Fresnel propagation Impulse Response was used'
        Lout = Lin;
    end
    typeOfpropagation = 'Fresnel';
else
    [uout,Lout,xout]=propFF(uin,Lin,lambda,z); clear uin
    typeOfpropagation = 'Fraunhofer';
    fprintf('Fraunhofer propagation was used \n the array has been resized accordnigly \n')
end
return
