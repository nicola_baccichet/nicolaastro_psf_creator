function uin = expandArray(uin)
% checks if the array has an even number of pixels and expands it if
% necessary
first = @(v)v(1);
[M,~]=size(uin);
if first(factor(M))-2 ~=0
    uin(M+1,M+1) = 0;
end
return