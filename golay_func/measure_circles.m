function [centers,radii,q,p,Ruv,th] = measure_circles(MTF,lambda,dx,mincirc,maxcirc,show)
% measures the circles and gets the variables for golay measurements
% -------------------------------------------------------------------
% variables for the measurements
% mincirc = 50; maxcirc = 60; show = 'n';

% measure the circles
[centers, radii, ~] = imfindcircles(MTF,[mincirc maxcirc],...
    'Sensitivity',.95,'Method','PhaseCode','EdgeThreshold',0.1);
% plots circles on image
if strcmp(show,'yes')==1
    figure
    imshow(nthroot(MTF,3));
    hold on
    viscircles(centers,radii,'EdgeColor','r');
end
%
radii = radii.*dx;
% computes the Golay3 related variables
% variables names refer to drawing at page 50 Argos thesis
q = mode(radii);

diff = abs(centers(:,1)-centers(:,2));
acc = centers((diff==min(diff)),:);   % array center coords
sapcc = centers(radii==q,:);  % single apertures center coords
sapcc = sapcc(1,:);
p = sqrt( (acc(1)-sapcc(1))^2 + (acc(2)-sapcc(2))^2).*dx;

% effective diameter
Ruv = (sqrt(3)*p + sqrt(4*q^2 - p^2)) / 2;

% angular resolution
th = 1.22*lambda/Ruv;
