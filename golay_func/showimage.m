function handle = showimage(x,y,I,contrast)
% create an image of the function I in a new figure with a given contrast
handle = figure;
imagesc(x,y,I.^(1./contrast));
axis square;axis xy; %colormap('gray');
return
