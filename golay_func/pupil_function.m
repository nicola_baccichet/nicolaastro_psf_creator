function pupil = pupil_function(X,Y,aperture_diam,pairs_distance,coordinates)
% provides the pupil function mesh of the entrance pupil of N indentical 
% apertures with centers given by 'coordinates'
k = size(coordinates,1);
pupil = zeros(size(X));
for i = 1:k
    pupil = pupil + circ(radius(coordinates(i,1)*pairs_distance+X,...
        coordinates(i,2)*pairs_distance+Y)/(0.5*aperture_diam));
end