function [u2,x2,L2]=propTF(u1,L,lambda,z)
% propagation - transfer function approach - Fresnel pattern
% assumes same x and y side lengths and uniform sampling
% u1 : source plane field
% L : source and observation plane side length
% lambda : wavelegth
% z : propagation distance
% u2 : image plane field

M=size(u1,1);                           %get imput field array size
dx=L/M;                                 %sample interval
k = 2*pi/lambda;                        %wavenumber
fx=-1/(2*dx):1/L:1/(2*dx)-1/L;          %freq coordinates
[FX,FY]=meshgrid(fx,fx); clear fx
pdsz = 2^1;                             % size of the padded array (if needed)

% H=exp(-1i*pi*lambda*z*(FX.^2+FY.^2));             % simplified transfer function
H=exp(1i*k*z)*exp(-1i*pi*lambda*z*(FX.^2+FY.^2));   %full expression of transfer function
clear FX FY

H=fftshift(H);                              %shift transfer function
U1=fft2(fftshift(u1));  clear u1            %shift, fft source field

% with zero padding (needs to be checked)
% h = fft2(fftshift(H),M*pdsz,M*pdsz);
% H=ifftshift(ifft2(h));                                    %shift transfer function
% U1=fft2(fftshift(u1),M*pdsz,M*pdsz);  clear u1            %shift, fft source field

U2=H.*U1;	clear H U1
u2=ifftshift(ifft2(U2)); clear U2

M2 = size(u2,1);
L2 = L + dx*(M2-M);
% L2=L;
x2 = -L2/2:dx:L2/2-dx;
return
