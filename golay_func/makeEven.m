function newnumber = makeEven(number)
if mod(number,2)~=0
    newnumber = number-1;
else
    newnumber = number;
end