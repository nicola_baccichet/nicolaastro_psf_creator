function [imout]=load_sample(filename,M)
% M is the size of the mesh field
% loads the image specified by "filename" and zero-pads it to the size of
% the field M
A = imread(filename);
A = rgb2gray(A);
A = flipud(A);
N = size(A,1);
Ig = double(A);
Ig = Ig/max(max(Ig));
imout = sqrt(Ig);
imout = padarray(imout,[(M-N)/2 (M-N)/2]);
return